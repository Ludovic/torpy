# -*- coding: utf-8 -*-

# -----------------------------------------------------------------
# This file is a part of TorPy project.
# Name:         TorPy: vernacular Python Turtle
# Copyright:    (C) 2014-2017 TorPy authors
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------

"""
DESCRIPTION :
    contient l'interface graphique.
    La plupart des actions renvoient à des fonctions qui sont dans les
    modules utils_aaa.
"""

# importation des modules utiles :
from __future__ import division, print_function

# importation des modules perso :
import utils
import utils_functions
import utils_filesdirs
import utils_python

# PyQt5, PyQt4 ou PySide :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
elif utils.PYQT == 'PYQT4':
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
else:
    from PySide import QtCore, QtGui as QtWidgets, QtGui

# autres modules :
import os
import io
import json


class MainWindow(QtWidgets.QMainWindow):
    """
    LA FENÊTRE PRINCIPALE
    """
    def __init__(self, locale, translator, splash, parent=None):
        """
        mise en place de l'interface
        plus des variables utiles
        """
        super(MainWindow, self).__init__(parent)
        # i18n :
        self.translator = translator
        self.locale = locale
        # le dossier du logiciel et des fichiers :
        self.beginDir = QtCore.QDir.currentPath()
        self.workDir = QtCore.QDir.homePath()
        # le nom du fichier :
        self.fileName = ''
        # les filtres d'extensions pour ouvrir et enregistrer :
        translatedText0 = QtWidgets.QApplication.translate('main', 'Text files')
        translatedText1 = QtWidgets.QApplication.translate('main', 'All files')
        self.extension = utils_functions.u('{0}(*.txt);;{1}()').format(
            translatedText0, translatedText1)
        # pour esquiver le premier appel à textChanged :
        self.first = True
        # pour savoir si la Tortue est en cours d'exécution :
        self.isPlaying = False
        # création et remplissage du dossier temporaire :
        self.tempPath = utils_filesdirs.createTempAppDir(utils.PROGNAME)
        for fileName in ('torpy.py', 'turtle.cfg'):
            sourceFile = utils_functions.u('{0}/files/{1}').format(self.beginDir, fileName)
            destFile = utils_functions.u('{0}/{1}').format(self.tempPath, fileName)
            utils_filesdirs.removeAndCopy(sourceFile, destFile)
        # on copie la version localisée de importturtle.py mais avec le nom de base :
        localeFileName = utils_functions.doLocale(
            self.locale, 'files/importturtle', '.py',
            defaultFileName='files/importturtle_fr.py')
        sourceFile = utils_functions.u('{0}/{1}').format(self.beginDir, localeFileName)
        destFile = utils_functions.u('{0}/{1}').format(self.tempPath, 'importturtle.py')
        utils_filesdirs.removeAndCopy(sourceFile, destFile)
        # on ouvre le fichier torpy.py pour récupérer son contenu :
        sourceFileName = utils_functions.u('{0}/files/torpy.py').format(self.beginDir)
        self.torpyLines = utils_filesdirs.readTextFile(sourceFileName)
        # on lit le fichier de config :
        self.configDict = {}
        self.configDir, first = utils_filesdirs.createConfigAppDir(utils.PROGNAME)
        configFileName = self.configDir.canonicalPath() + '/config.json'
        try:
            if QtCore.QFile(configFileName).exists():
                if utils.PYTHONVERSION >= 30:
                    configFile = open(configFileName, newline='', encoding='utf-8')
                else:
                    configFile = open(configFileName, 'rb')
                self.configDict = json.load(configFile)
                configFile.close()
        except:
            self.configDict = {}
        # récupération ou initialisation des valeurs :
        for key in utils.DEFAULTCONFIG:
            if not(key in self.configDict):
                self.configDict[key] = utils.DEFAULTCONFIG[key]
        # on vérifie l'existence des fichiers :
        lastFiles = []
        for fileName in self.configDict['LASTFILES']:
            if QtCore.QFileInfo(fileName).isFile():
                lastFiles.append(fileName)
        self.configDict['LASTFILES'] = lastFiles
        # mise en place de l'interface :
        self.createInterface()
        self.updateTitle()
        self.actionSaveImages.setEnabled(False)
        # redimensionnement de la fenêtre :
        rect = QtWidgets.QApplication.desktop().availableGeometry()
        w, h = int(rect.width()), int(rect.height())
        if w > 550:
            w = 550
        self.resize(w, h)
        splash.finish(self)

    def closeEvent(self, event):
        self.stopApp()
        if not(self.testMustSave()):
            event.ignore()
            return
        # on écrit le fichier de config :
        configFileName = self.configDir.canonicalPath() + '/config.json'
        if utils.PYTHONVERSION >= 30:
            configFile = open(configFileName, 'w', encoding='utf-8')
        else:
            configFile = open(configFileName, 'wb')
        # on ne garde que 10 fichiers :
        self.configDict['LASTFILES'] = self.configDict['LASTFILES'][:10]
        json.dump(self.configDict, configFile, indent=4)
        configFile.close()
        # suppression du dossier temporaire :
        utils_filesdirs.emptyDir(QtCore.QDir.tempPath() + '/' + utils.PROGNAME)
        tempDir = QtCore.QDir.temp()
        tempDir.rmdir(utils.PROGNAME)
        event.accept()

    def newFile(self):
        if not(self.testMustSave()):
            return
        self.turtleEditor.clear()
        self.fileName = ''
        self.updateTitle()
        self.actionSaveImages.setEnabled(False)

    def openFile(self, fileName=None):
        """
        ouverture d'un fichier.
        Elle peut être appelée par l'action "ouvrir",
        mais aussi en cliquant sur un fichier récent,
        ou enfin par le dernier paramètre passé au lancement.
        La variable "who" permet de récupérer qui a demandé
        l'ouverture de fichier.
        """
        if not(self.testMustSave()):
            return
        if fileName:
            # on vérifie l'existence du fichier car il peut y avoir
            # d'autres paramètres dans la ligne de commande :
            who = 'param'
            if not(QtCore.QFileInfo(fileName).isFile()):
                return
        elif self.sender() != self.actionOpen:
            # on a demandé un fichier récent :
            who = 'recent'
            fileName = self.sender().data()
        else:
            # on commence par sélectionner le fichier :
            who = 'action'
            title = QtWidgets.QApplication.translate('main', 'Open a File')
            proposedName = utils_functions.u('{0}/').format(self.workDir)
            fileName = QtWidgets.QFileDialog.getOpenFileName(
                self, title, proposedName, self.extension)
            # pour PySide :
            if isinstance(fileName, tuple):
                fileName = fileName[0]
        if fileName == '':
            return
        # on y va :
        utils_functions.doWaitCursor()
        try:
            self.workDir = QtCore.QFileInfo(fileName).absolutePath()
            fileContent = utils_filesdirs.readTextFile(fileName)
            self.turtleEditor.setPlainText(fileContent)
            self.fileName = fileName
            if who != 'recent':
                # on met à jour les fichiers récents :
                if (fileName in self.configDict['LASTFILES']):
                    self.configDict['LASTFILES'].remove(fileName)
                self.configDict['LASTFILES'].insert(0, fileName)
                self.updateLastFilesMenu()
            self.updateTitle()
            self.actionSaveImages.setEnabled(False)
        finally:
            utils_functions.restoreCursor()

    def saveFile(self):
        """
        enregistrement d'un fichier.
        """
        if (self.sender() == self.actionSaveAs) or (self.fileName == ''):
            # choix du nom du fichier :
            title = QtWidgets.QApplication.translate('main', 'Save a File')
            fileName = QtWidgets.QFileDialog.getSaveFileName(
                self, title, self.workDir, self.extension)
            # pour PySide :
            if isinstance(fileName, tuple):
                fileName = fileName[0]
            if fileName == '':
                return
            self.workDir = QtCore.QFileInfo(fileName).absolutePath()
            self.fileName = fileName
            # on met à jour les fichiers récents :
            if (fileName in self.configDict['LASTFILES']):
                self.configDict['LASTFILES'].remove(fileName)
            self.configDict['LASTFILES'].insert(0, fileName)
            self.updateLastFilesMenu()
        # on y va :
        utils_functions.doWaitCursor()
        try:
            lines = self.turtleEditor.toPlainText()
            QtCore.QFile(self.fileName).remove()
            outFile = io.open(self.fileName, 'wt', encoding='utf-8')
            for line in lines:
                outFile.write(line)
            outFile.close()
            self.updateTitle()
        finally:
            utils_functions.restoreCursor()

    def updateLastFilesMenu(self):
        """
        mise à jour du menu des fichiers récents
        """
        self.lastFilesMenu.clear()
        for fileName in self.configDict['LASTFILES']:
            newAction = QtWidgets.QAction(fileName, self)
            newAction.triggered.connect(self.openFile)
            newAction.setData(fileName)
            self.lastFilesMenu.addAction(newAction)

    def updateTitle(self, mustSave=False):
        """
        mise à jour du titre de la fenêtre.
        Suivant que le fichier est enregistré, a été modifié, ...
        """
        self.mustSave = mustSave
        if mustSave:
            mustSaveText = '*'
        else:
            mustSaveText = ''
        if self.fileName == '':
            fileNameText = QtWidgets.QApplication.translate('main', 'No name')
        else:
            fileNameText = self.fileName
        title = utils_functions.u('{0} {1}  {2}[{3}]').format(
            utils.PROGTITLE, utils.PROGVERSION, mustSaveText, fileNameText)
        self.setWindowTitle(title)

    def textChanged(self):
        """
        pour prendre en compte dans le titre le fait que le texte a été modifié.
        """
        if self.first:
            self.first = False
            return
        self.updateTitle(mustSave=True)

    def testMustSave(self):
        """
        pour savoir s'il faut proposer d'enregistrer le fichier.
        """
        result = True
        if self.mustSave:
            message = QtWidgets.QApplication.translate(
                'main',
                'The file has been modified.\n'
                'Do you want to save your changes?')
            reponseMustSave = utils_functions.messageBox(
                self, level='warning', message=message,
                buttons=['Save', 'Discard', 'Cancel'])
            if reponseMustSave == QtWidgets.QMessageBox.Cancel:
                result = False
            elif reponseMustSave == QtWidgets.QMessageBox.Save:
                self.saveFile()
        return result

    def doCreateDesktopFileLinux(self):
        """
        Sous GNU/Linux, propose de créer un fichier .desktop
        pour lancer le logiciel.
        """
        title = QtWidgets.QApplication.translate(
            'main', 'Choose the Directory where the desktop file will be created')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self,
            title,
            QtCore.QDir.homePath(),
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory != '':
            result = utils_filesdirs.createDesktopFile(
                self, directory, utils.PROGNAME, 'torpy')

    def about(self):
        """
        affichage de la fenêtre À propos.
        """
        import utils_about
        aboutdialog = utils_about.AboutDlg(
            self, self.locale, icon='./images/splash.png')
        aboutdialog.resize(700, 500)
        aboutdialog.exec_()

    def progInstructions(self):
        """
        affichage ou non des instructions.
        """
        if self.actionInstructions.isChecked():
            self.stackedWidget.setCurrentIndex(1)
        else:
            self.stackedWidget.setCurrentIndex(0)

    def projectPage(self):
        """
        ouvre la page du projet dans le navigateur.
        """
        import utils_about
        utils_about.webHelpInBrowser(utils.HELPPAGE)

    def run(self):
        """
        lancement de la tortue.
        Le texte saisi par l'utilisateur est inséré dans le fichier torpy.py.
        Ce fichier est alors copié dans le dossier temporaire
        et c'est lui qui est ensuite exécuté.
        """
        if utils_python.SOFTS['python3'][0]:
            softName = 'python3'
        elif utils_python.SOFTS['python'][0]:
            softName = 'python'
        else:
            return
        # on remplace la ligne repère par le texte saisi par l'utilisateur :
        lines = self.torpyLines.replace('# USER INSTRUCTIONS', self.turtleEditor.toPlainText())
        # on enregistre dans temp :
        tempFileName = utils_functions.u('{0}/torpy.py').format(self.tempPath)
        if QtCore.QFile(tempFileName).exists():
            QtCore.QFile(tempFileName).remove()
        tempFile = io.open(tempFileName, 'wt', encoding='utf-8')
        for line in lines:
            tempFile.write(line)
        tempFile.close()
        # on execute le fichier :
        self.logEditor.clear()
        self.startApp(softName, args=[tempFileName, ])

    def startApp(self, softName, args=[]):
        """
        execution du QProcess
        """
        if self.isPlaying:
            return True
        # On parse la stdout et stderr au même endroit, donc on demande
        # à fusionnner les 2 flux :
        self.process.setProcessChannelMode(QtCore.QProcess.MergedChannels)
        self.process.start(utils_python.SOFTS[softName][1], args)
        if not self.process.waitForStarted(3000):
            return False
        self.setEnabled(False)
        self.isPlaying = True
        return True

    def stopApp(self):
        """
        pour arrêter le QProcess
        """
        if self.process.state() == QtCore.QProcess.NotRunning:
            return True
        if not self.isPlaying:
            return True
        self.process.close()
        return True

    def catchOutput(self):
        """
        affichage des retours console dans le logEditor.
        """
        while self.process.canReadLine():
            text = utils_functions.u(self.process.readLine())
            if len(text) > 1:
                self.logEditor.append(text[0:-1])

    def appEnded(self, exitCode, exitStatus):
        """
        le QProcess est terminé.
        """
        self.setEnabled(True)
        self.actionSaveImages.setEnabled(self.fileName != '')
        self.isPlaying = False

    def doImages(self):
        """
        fabrication des images.
        Possible seulement si le fichier est enregistré.
        Une image ps est automatiquement créée depuis turtle.
        Si pstoedit est détecté, on fabrique aussi un fichier svg.
        """
        if self.fileName == '':
            return
        utils_functions.doWaitCursor()
        try:
            extensions = ['eps', ]
            if utils_python.SOFTS['pstoedit'][0]:
                import subprocess
                sourceFile = utils_functions.u('{0}/temp.eps').format(self.tempPath)
                # for extension in ('svg', 'pdf',):
                for extension in ('svg',):
                    destFile = utils_functions.u('{0}/temp.{1}').format(self.tempPath, extension)
                    # on supprime les fichiers précédents :
                    if QtCore.QFile(destFile).exists():
                        QtCore.QFile(destFile).remove()
                    if extension == 'svg':
                        convertionFormat = 'plot-svg'
                    elif extension == 'pdf':
                        convertionFormat = 'gs:pdfwrite'
                    commandLine = 'pstoedit -f {0} {1} {2}'.format(
                        convertionFormat, sourceFile, destFile)
                    try:
                        retcode = subprocess.call(commandLine, shell=True)
                    except:
                        print('except in pstoedit', extension)
                    if QtCore.QFile(destFile).exists():
                        extensions.append(extension)
            # on copie les fichiers créés à côté du fichier torpy initial :
            for extension in extensions:
                sourceFile = utils_functions.u('{0}/temp.{1}').format(self.tempPath, extension)
                destFile = utils_functions.u('{0}/{1}.{2}').format(
                    self.workDir,
                    QtCore.QFileInfo(self.fileName).baseName(),
                    extension)
                utils_filesdirs.removeAndCopy(sourceFile, destFile)
        finally:
            utils_functions.restoreCursor()
            utils_functions.afficheMsgFinOpenDir(self.workDir, message=self.workDir)

    def createInterface(self):
        # le process d'exécution de la tortue :
        self.process = QtCore.QProcess(self)
        self.process.readyReadStandardOutput.connect(self.catchOutput)
        self.process.finished.connect(self.appEnded)

        # un TurtleEditor pour écrire les instructions :
        self.turtleEditor = utils_python.TurtleEditor(self)
        self.turtleEditor.textChanged.connect(self.textChanged)
        self.turtleEditor.setMinimumSize(400, 300)

        # un deuxième pour afficher l'aide :
        self.instructionsTurtleEditor = utils_python.TurtleEditor(self, readOnly=True)
        instructionsFileName = utils_functions.doLocale(
            self.locale, 'translations/instructions', '',
            defaultFileName='translations/instructions_fr')
        instructions = utils_filesdirs.readTextFile(instructionsFileName)
        self.instructionsTurtleEditor.setPlainText(instructions)

        # affichage des retours de la console :
        self.logEditor = QtWidgets.QTextEdit()
        self.logEditor.setReadOnly(True)
        self.logEditor.setMaximumHeight(100)
        turtleLayout = QtWidgets.QVBoxLayout()
        turtleLayout.addWidget(self.turtleEditor)
        turtleLayout.addWidget(self.logEditor)
        turtleGroupBox = QtWidgets.QGroupBox()
        turtleGroupBox.setLayout(turtleLayout)

        # le stackedWidget est le widget central :
        self.stackedWidget = QtWidgets.QStackedWidget()
        self.stackedWidget.addWidget(turtleGroupBox)
        self.stackedWidget.addWidget(self.instructionsTurtleEditor)
        self.setCentralWidget(self.stackedWidget)

        # le reste de l'interface :
        self.statusbar = QtWidgets.QStatusBar()
        self.setStatusBar(self.statusbar)
        self.toolBar = QtWidgets.QToolBar(QtWidgets.QApplication.translate('main', 'Base Bar'))
        self.toolBar.setIconSize(QtCore.QSize(32, 32))
        self.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
        self.createActions()
        self.createMenusAndButtons()
        self.updateLastFilesMenu()

    def createActions(self):
        self.actionQuit = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'E&xit'), self,
            icon=QtGui.QIcon('images/application-exit.png'),
            shortcut=QtWidgets.QApplication.translate('main', 'Ctrl+Q'))
        self.actionQuit.triggered.connect(self.close)

        self.actionNew = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', '&New'), self,
            icon=QtGui.QIcon('images/document-new.png'),
            shortcut=QtWidgets.QApplication.translate('main', 'Ctrl+N'),
            statusTip=QtWidgets.QApplication.translate(
                'main', 'New file'))
        self.actionNew.triggered.connect(self.newFile)

        self.actionOpen = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', '&Open'), self,
            icon=QtGui.QIcon('images/document-open.png'),
            shortcut=QtWidgets.QApplication.translate('main', 'Ctrl+O'),
            statusTip=QtWidgets.QApplication.translate(
                'main', 'Open a file'))
        self.actionOpen.triggered.connect(self.openFile)

        self.actionSave = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', '&Save'), self,
            icon=QtGui.QIcon('images/document-save.png'),
            shortcut=QtWidgets.QApplication.translate('main', 'Ctrl+S'),
            statusTip=QtWidgets.QApplication.translate(
                'main', 'Save a file'))
        self.actionSave.triggered.connect(self.saveFile)

        self.actionSaveAs = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Save &as'), self,
            icon=QtGui.QIcon('images/document-save-as.png'),
            statusTip=QtWidgets.QApplication.translate(
                'main', 'Save the file as...'))
        self.actionSaveAs.triggered.connect(self.saveFile)

        self.actionRun = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', '&Run the Turtle'), self,
            icon=QtGui.QIcon('images/run-build.png'),
            statusTip=QtWidgets.QApplication.translate('main', 'Execute instructions by the Turtle'))
        self.actionRun.triggered.connect(self.run)

        self.actionSaveImages = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Save &images'), self,
            icon=QtGui.QIcon('images/pixmap.png'),
            statusTip=QtWidgets.QApplication.translate('main', 'Save the Turtle window as images files'))
        self.actionSaveImages.triggered.connect(self.doImages)

        self.actionCreateDesktopFileLinux = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Create a launcher'), self,
            icon=QtGui.QIcon('images/logo_linux.png'),
            statusTip=QtWidgets.QApplication.translate(
                'main', 'To create a launcher file (*.desktop) in the folder of your choice'))
        self.actionCreateDesktopFileLinux.triggered.connect(self.doCreateDesktopFileLinux)

        self.actionInstructions = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', '&Instructions'), self,
            icon=QtGui.QIcon('images/documentation.png'),
            shortcut=QtWidgets.QApplication.translate('main', 'F1'),
            statusTip=QtWidgets.QApplication.translate('main', 'Display list of available instructions'),
            checkable=True, checked=False)
        self.actionInstructions.triggered.connect(self.progInstructions)

        self.actionProjectPage = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', '&Project Web Page'), self,
            icon=QtGui.QIcon('images/help.png'),
            statusTip=QtWidgets.QApplication.translate('main', 'View project website in your browser'))
        self.actionProjectPage.triggered.connect(self.projectPage)

        translatedText = QtWidgets.QApplication.translate('main', 'About {0}')
        translatedText = utils_functions.u(translatedText).format(utils.PROGTITLE)
        self.actionAbout = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', '&About'), self,
            icon=QtGui.QIcon('images/help-about.png'),
            statusTip=translatedText)
        self.actionAbout.triggered.connect(self.about)

    def createMenusAndButtons(self):
        """
        blablabla
        """
        self.fileMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', '&File'), self)
        self.fileMenu.addAction(self.actionNew)
        self.fileMenu.addAction(self.actionOpen)
        self.lastFilesMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', '&Recent Files'), self)
        self.lastFilesMenu.setIcon(QtGui.QIcon('images/document-open-recent.png'))
        self.fileMenu.addMenu(self.lastFilesMenu)
        self.fileMenu.addAction(self.actionSave)
        self.fileMenu.addAction(self.actionSaveAs)
        if utils.OS_NAME[0] == 'linux':
            self.fileMenu.addSeparator()
            self.fileMenu.addAction(self.actionCreateDesktopFileLinux)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.actionQuit)

        self.runMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', '&Run'), self)
        self.runMenu.addAction(self.actionRun)
        self.runMenu.addAction(self.actionSaveImages)

        self.helpMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', '&Help'), self)
        self.helpMenu.addAction(self.actionInstructions)
        self.helpMenu.addAction(self.actionProjectPage)
        self.helpMenu.addAction(self.actionAbout)

        self.menuBar().addMenu(self.fileMenu)
        self.menuBar().addMenu(self.runMenu)
        self.menuBar().addMenu(self.helpMenu)

        self.toolBar.addAction(self.actionQuit)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionNew)
        self.toolBar.addAction(self.actionOpen)
        self.toolBar.addAction(self.actionSave)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionRun)
        self.toolBar.addAction(self.actionSaveImages)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionInstructions)
