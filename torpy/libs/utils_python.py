# -*- coding: utf-8 -*-

# -----------------------------------------------------------------
# This file is a part of TorPy project.
# Name:         TorPy: vernacular Python Turtle
# Copyright:    (C) 2014-2017 TorPy authors
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------

"""
DESCRIPTION :
    execution d'un fichier python
    et aussi un QSyntaxHighlighter
    http://dvlabs.tippingpoint.com/blog/2012/02/26/mindshare-syntax-coloring
"""

# importation des modules utiles :
from __future__ import division, print_function
import sys
import os

import utils
import utils_functions
import utils_filesdirs

# PyQt5, PyQt4 ou PySide :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
elif utils.PYQT == 'PYQT4':
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
else:
    from PySide import QtCore, QtGui as QtWidgets, QtGui


# variables globales pour savoir si ça marche :
SOFTS = {
    'python3': [False, ''],
    'python': [False, ''],
    'pstoedit': [False, ''],
    }


def testSofts():
    """
    D'une part on teste si les logiciels sont installés, on récupère les
    chemins et on les ajoute au path.
    Si c'est Windows, il faut vérifier aussi la base de registre.
    """
    global SOFTS
    if utils.PYTHONVERSION >= 30:
        softName = 'python3'
        # pas besoin de Python 2 :
        del SOFTS['python']
    else:
        softName = 'python'
    fileName = sys.executable
    if not(fileName in ('', None)):
        SOFTS[softName][1] = fileName
        SOFTS[softName][0] = True
    if sys.platform == 'win32':
        if utils.PYTHONVERSION >= 30:
            import winreg
        else:
            import _winreg as winreg
        for softName in SOFTS:
            if SOFTS[softName][0]:
                continue
            if softName == 'python':
                try:
                    # on cherche où est python via la base de registre :
                    key = winreg.OpenKey(
                        winreg.HKEY_LOCAL_MACHINE,
                        'SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\Python.exe',
                        0, winreg.KEY_READ)
                    (valeur, typevaleur) = winreg.QueryValueEx(key, '')
                    winreg.CloseKey(key)
                    app = valeur
                except:
                    app = ''
                # les windowsiens ont peur de la console :
                app = app.replace('ython.exe', 'ythonw.exe')
                SOFTS[softName][1] = app
            if SOFTS[softName][1] != '':
                SOFTS[softName][0] = True
            else:
                SOFTS[softName][0] = False
    else:
        for softName in SOFTS:
            if SOFTS[softName][0]:
                continue
            # chemins possibles :
            paths = ('/opt', '/usr/bin', )
            # on cherche où est le binaire :
            for pathName in paths:
                fileName = os.path.join(pathName, softName)
                if os.path.exists(fileName):
                    SOFTS[softName][1] = fileName
                    break
            if SOFTS[softName][1] != '':
                SOFTS[softName][0] = True
    print('SOFTS TEST:')
    for softName in SOFTS:
        print(utils_functions.u('{0} : {1}').format(
            softName, SOFTS[softName]))

testSofts()


class TurtleEditor(QtWidgets.QTextEdit):
    """
    un éditeur avec coloration syntaxique
    et reconnaissance des mots traduits.
    """
    def __init__(self, parent=None, readOnly=False):
        super(TurtleEditor, self).__init__(parent)
        self.main = parent
        self.ctrl = False
        self.readOnly = readOnly
        if readOnly:
            self.setReadOnly(True)
        self.setFontFamily('DejaVu Sans Mono')
        self.zoom = 8
        self.zoomIn(1.5)
        self.highlighter = MyHighlighter(
            self.document(), tempPath=self.main.tempPath)
        self.setAcceptDrops(True)

    def keyPressEvent(self, event):
        if not(self.readOnly):
            if event.key() == QtCore.Qt.Key_Control:
                self.ctrl = True
            elif event.key() == QtCore.Qt.Key_Tab:
                self.textCursor().insertText('    ')
                return
        QtWidgets.QTextEdit.keyPressEvent(self, event)

    def keyReleaseEvent(self, event):
        if not(self.readOnly):
            if event.key() == QtCore.Qt.Key_Control:
                self.ctrl = False
        QtWidgets.QTextEdit.keyReleaseEvent(self, event)

    def wheelEvent(self, event):
        mini = 6
        maxi = 100
        coeff = 1.5
        if not(self.readOnly):
            if self.ctrl:
                if event.delta() > 0:
                    self.zoom += 1
                    if self.zoom > maxi:
                        self.zoom = maxi
                    else:
                        self.zoomIn(coeff)
                elif event.delta() < 0:
                    self.zoom -= 1
                    if self.zoom < mini:
                        self.zoom = mini
                    else:
                        self.zoomOut(coeff)
        QtWidgets.QTextEdit.wheelEvent(self, event)

    def dragEnterEvent(self, event):
        """
        on accepte le drag seulement si c'est un fichier *.txt
        """
        accept = False
        if event.mimeData().hasUrls:
            try:
                fileName = event.mimeData().urls()[0].toLocalFile()
                extension = QtCore.QFileInfo(
                    fileName).completeSuffix().lower()
                if extension == 'txt':
                    accept = True
            except:
                pass
        if accept:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        """
        on demande l'ouverture du fichier
        """
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            try:
                fileName = utils_functions.u(
                    event.mimeData().urls()[0].toLocalFile())
                self.main.openFile(fileName)
            except:
                pass
        else:
            event.ignore()


class MyHighlighter(QtGui.QSyntaxHighlighter):
    """
    Syntax highlighter for the Python language.
    https://wiki.python.org/moin/PyQt/Python syntax highlighting
    Ajout de l'import du module importturtle.
    Correction de text.length() en len(text).
    """

    def applyFormat(color, style=''):
        """Return a QTextCharFormat with the given attributes.
        """
        _color = QtGui.QColor()
        _color.setNamedColor(color)

        _format = QtGui.QTextCharFormat()
        _format.setForeground(_color)
        if 'bold' in style:
            _format.setFontWeight(QtGui.QFont.Bold)
        if 'italic' in style:
            _format.setFontItalic(True)
        return _format

    # Syntax styles that can be shared by all languages
    STYLES = {
        'keyword': applyFormat('blue'),
        'operator': applyFormat('red'),
        'brace': applyFormat('maroon'),
        'defclass': applyFormat('blue', 'bold'),
        'string': applyFormat('magenta'),
        'string2': applyFormat('gray', 'italic'),
        'comment': applyFormat('gray', 'italic'),
        'self': applyFormat('green', 'bold'),
        'numbers': applyFormat('darkgoldenrod'),
    }

    # Python keywords
    keywords = [
        'and', 'assert', 'break', 'class', 'continue', 'def',
        'del', 'elif', 'else', 'except', 'exec', 'finally',
        'for', 'from', 'global', 'if', 'import', 'in',
        'is', 'lambda', 'not', 'or', 'pass', 'print',
        'raise', 'return', 'try', 'while', 'yield',
        'None', 'True', 'False',
    ]

    # Python operators
    operators = [
        '=',
        # Comparison
        '==', '!=', '<', '<=', '>', '>=',
        # Arithmetic
        '\+', '-', '\*', '/', '//', '\%', '\*\*',
        # In-place
        '\+=', '-=', '\*=', '/=', '\%=',
        # Bitwise
        '\^', '\|', '\&', '\~', '>>', '<<',
    ]

    # Python braces
    braces = [
        '\{', '\}', '\(', '\)', '\[', '\]',
    ]

    def __init__(self, document, tempPath=''):
        QtGui.QSyntaxHighlighter.__init__(self, document)
        self.tri_single = (QtCore.QRegExp("'''"), 1, self.STYLES['string2'])
        self.tri_double = (QtCore.QRegExp('"""'), 2, self.STYLES['string2'])

        rules = []

        if tempPath != '':
            sys.path.insert(0, tempPath)
            import importturtle
            rules += [(r'\b%s\b' % w, 0, self.STYLES['keyword'])
                      for w in importturtle.keywords]

        # Keyword, operator, and brace rules
        rules += [(r'\b%s\b' % w, 0, self.STYLES['keyword'])
                  for w in MyHighlighter.keywords]
        rules += [(r'%s' % o, 0, self.STYLES['operator'])
                  for o in MyHighlighter.operators]
        rules += [(r'%s' % b, 0, self.STYLES['brace'])
                  for b in MyHighlighter.braces]

        # All other rules
        rules += [
            # 'self'
            (r'\bself\b', 0, self.STYLES['self']),

            # Double-quoted string, possibly containing escape sequences
            (r'"[^"\\]*(\\.[^"\\]*)*"', 0, self.STYLES['string']),
            # Single-quoted string, possibly containing escape sequences
            (r"'[^'\\]*(\\.[^'\\]*)*'", 0, self.STYLES['string']),

            # 'def' followed by an identifier
            (r'\bdef\b\s*(\w+)', 1, self.STYLES['defclass']),
            # 'class' followed by an identifier
            (r'\bclass\b\s*(\w+)', 1, self.STYLES['defclass']),

            # From '#' until a newline
            (r'#[^\n]*', 0, self.STYLES['comment']),

            # Numeric literals
            (r'\b[+-]?[0-9]+[lL]?\b', 0, self.STYLES['numbers']),
            (r'\b[+-]?0[xX][0-9A-Fa-f]+[lL]?\b', 0, self.STYLES['numbers']),
            (r'\b[+-]?[0-9]+(?:\.[0-9]+)?(?:[eE][+-]?[0-9]+)?\b', 0, self.STYLES['numbers']),
        ]

        # Build a QRegExp for each pattern
        self.rules = [(QtCore.QRegExp(pat), index, fmt)
                      for (pat, index, fmt) in rules]

    def highlightBlock(self, text):
        """Apply syntax highlighting to the given block of text.
        """
        # Do other syntax formatting
        for expression, nth, format in self.rules:
            index = expression.indexIn(text, 0)
            while index >= 0:
                # We actually want the index of the nth match
                index = expression.pos(nth)
                length = len(expression.cap(nth))
                self.setFormat(index, length, format)
                index = expression.indexIn(text, index + length)

        self.setCurrentBlockState(0)

        # Do multi-line strings
        in_multiline = self.match_multiline(text, *self.tri_single)
        if not in_multiline:
            in_multiline = self.match_multiline(text, *self.tri_double)

    def match_multiline(self, text, delimiter, in_state, style):
        """Do highlighting of multi-line strings. ``delimiter`` should be a
        ``QRegExp`` for triple-single-quotes or triple-double-quotes, and
        ``in_state`` should be a unique integer to represent the corresponding
        state changes when inside those strings. Returns True if we're still
        inside a multi-line string when this function is finished.
        """
        # return False
        # If inside triple-single quotes, start at 0
        if self.previousBlockState() == in_state:
            start = 0
            add = 0
        # Otherwise, look for the delimiter on this line
        else:
            start = delimiter.indexIn(text)
            # Move past this match
            add = delimiter.matchedLength()

        # As long as there's a delimiter match on this line...
        while start >= 0:
            # Look for the ending delimiter
            end = delimiter.indexIn(text, start + add)
            # Ending delimiter on this line?
            if end >= add:
                length = end - start + add + delimiter.matchedLength()
                self.setCurrentBlockState(0)
            # No; multi-line string
            else:
                self.setCurrentBlockState(in_state)
                length = len(text) - start + add
            # Apply formatting
            self.setFormat(start, length, style)
            # Look for the next match
            start = delimiter.indexIn(text, start + length)

        # Return True if still inside a multi-line string, False otherwise
        if self.currentBlockState() == in_state:
            return True
        else:
            return False
