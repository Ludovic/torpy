#!/usr/bin/env python
# -*- coding: utf-8 -*-


# importation des modules utiles :
# import of useful modules:
from __future__ import division, print_function
import sys
import os

# récupération du chemin :
# recovery path:
HERE = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, HERE)
os.chdir(HERE)

# on importe le module de traduction de turtle :
# we import the turtle translation module:
from importturtle import *


# USER INSTRUCTIONS


# création du fichier eps :
# creation of the eps file:
ts = turtle.getscreen()
ts.getcanvas().postscript(file='temp.eps')

# un clic ferme la fenêtre turtle :
# click closes the turtle window:
turtle.exitonclick()
